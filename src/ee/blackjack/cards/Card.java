package ee.blackjack.cards;

import ee.blackjack.enums.CardName;
import ee.blackjack.enums.Suit;

/**
 * Created by Sannu on 12.10.2016.
 */
public class Card {

    private CardName cardName;
    private int value;
    private Suit suit;

    public Card(int value, Suit suit, CardName cardName) {
        this.value = value;
        this.suit = suit;
        this.cardName = cardName;
    }

    public int getValue() {
        return value;
    }

    public Suit getSuit() {
        return suit;
    }

    public CardName getCardName() {
        return cardName;
    }

    public void setCardName(CardName cardName) {
        this.cardName = cardName;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setSuit(Suit suit) {
        this.suit = suit;
    }

    @Override
    public String toString() {
        return "Card{" +
                "cardName=" + cardName +
                ", value=" + value +
                ", suit=" + suit +
                '}';
    }
}
