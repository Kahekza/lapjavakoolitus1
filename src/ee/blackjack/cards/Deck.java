package ee.blackjack.cards;

import ee.blackjack.enums.CardName;
import ee.blackjack.enums.Suit;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by karl-martin on 10/13/16.
 */
public class Deck extends LinkedList<Card> {

    private final List<CardName> cardNames;

    public Deck() {
        cardNames = Arrays.asList(CardName.values());
        generateCardDeck();
    }

    public List generateCardDeck() {
        for(int i = 0; i < 13; i++) {
            int value;
            if (i > 9) {
                value = 10;
            } else {
                value = i+1;
            }
            add(new Card(value, Suit.CLUBS, cardNames.get(i)));
            add(new Card(value, Suit.DIAMONDS, cardNames.get(i)));
            add(new Card(value, Suit.SPADES, cardNames.get(i)));
            add(new Card(value, Suit.HEARTS, cardNames.get(i)));
        }
        Collections.shuffle(this);
        return this;
    }
}
