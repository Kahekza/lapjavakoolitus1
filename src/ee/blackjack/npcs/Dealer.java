package ee.blackjack.npcs;

import ee.blackjack.cards.Card;
import ee.blackjack.cards.Deck;

/**
 * Created by Sannu on 12.10.2016.
 */
public class Dealer {


    private Deck deck;

    public Dealer() {
        deck = new Deck();
    }


    public Card pullCardFromDeck() {
        Card drawnCard = deck.get(0);
        deck.remove(drawnCard);
        return drawnCard;
    }

    public void newGame() {
        deck = new Deck();
    }
}
