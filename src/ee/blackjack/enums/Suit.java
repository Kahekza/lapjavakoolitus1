package ee.blackjack.enums;

/**
 * Created by Sannu on 12.10.2016.
 */
public enum Suit {
    SPADES, HEARTS, DIAMONDS, CLUBS
}
