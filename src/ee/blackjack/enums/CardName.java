package ee.blackjack.enums;

/**
 * Created by karl-martin on 10/13/16.
 */
public enum CardName {
    ACE,
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN,
    JACK,
    QUEEN,
    KING
}
